package byog.Core;

import byog.TileEngine.TERenderer;
import byog.TileEngine.TETile;
import edu.princeton.cs.introcs.StdDraw;
import java.util.Random;


public class Game {
    TERenderer ter = new TERenderer();
    public static final int WIDTH = 80;
    public static final int HEIGHT = 45;

    public Random RANDOM;
    public TETile[][] WORLD;
    public TERenderer TER = new TERenderer();

    /**
     * Method used for playing a fresh game. The game should start from the main menu.
     */

    public void playWithKeyboard() {
        //start the player at a random place based on the seed.

        int[] playerXY = MapGeneratorNew.makeRandomPlayerPosition(RANDOM, WORLD);
        Player player = new Player(playerXY, WORLD);
        TER.renderFrame(WORLD);
        player.move();
        player.quit();
//        moveAndCursor(player); //player 1 moves with "w,a,s,d" , player2 moves with "i,j,k,l"
//        MapGeneratorNew.displayCursorLocation(WORLD);
    }

    public void moveAndCursor(Player players) {

        while (true) {

            if (StdDraw.hasNextKeyTyped()) {
                Character direction = StdDraw.nextKeyTyped();
                if (direction.equals(':')) {
                    players.quit();
                }
                players.moveHelper(direction);
                TER.renderFrame(WORLD);
            }
            MapGeneratorNew.displayCursorLocation(WORLD);
        }
    }


    /**
     * Method used for autograding and testing the game code. The input string will be a series
     * of characters (for example, "n123sswwdasdassadwas", "n123sss:q", "lwww". The game should
     * behave exactly as if the user typed these characters into the game after playing
     * playWithKeyboard. If the string ends in ":q", the same world should be returned as if the
     * string did not end with q. For example "n123sss" and "n123sss:q" should return the same
     * world. However, the behavior is slightly different. After playing with "n123sss:q", the game
     * should save, and thus if we then called playWithInputString with the string "l", we'd expect
     * to get the exact same world back again, since this corresponds to loading the saved game.
     *
     * @param input the input string to feed to your program
     * @return the 2D TETile[][] representing the state of the world
     */
    public TETile[][] playWithInputString(String input) {

        String integerInput = input.substring(1, input.length() - 1);
        long longSeed = Long.parseLong(integerInput);
        RANDOM = new Random(longSeed);
        TETile[][] world = MapGeneratorNew.makeWorld(RANDOM);
        WORLD = world;
        return world;
    }

}