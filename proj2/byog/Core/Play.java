package byog.Core;

import edu.princeton.cs.introcs.StdDraw;

import java.awt.Color;
import java.awt.Font;
import java.util.Random;
import byog.TileEngine.TETile;
import byog.TileEngine.Tileset;
import byog.TileEngine.TERenderer;
import java.io.Serializable;


public class Play implements Serializable {

    private int width;
    private int height;
    TERenderer TER = new TERenderer();


    public Play(int width, int height) {

        this.width = width;
        this.height = height;
        StdDraw.setCanvasSize(this.width * 20, this.height * 20);
        Font font = new Font("Monaco", Font.BOLD, 30);
        StdDraw.setFont(font);
        StdDraw.setXscale(0, this.width);
        StdDraw.setYscale(0, this.height);
        StdDraw.clear(Color.BLUE);
        StdDraw.enableDoubleBuffering();

        //rand = new Random(seed);
    }

    public void drawFirstFrame() {
        StdDraw.clear(Color.BLACK);
        Font font = new Font("Monaco", Font.BOLD, 18);
        StdDraw.setFont(font);
        StdDraw.setPenColor(StdDraw.GREEN);
        StdDraw.text(20, 28, "CS 61B: THE GAME");
        StdDraw.text(20, 25, "T & O PRODUCTIONS");
        StdDraw.text(20, 17, "NEW GAME (N)");
        StdDraw.text(20, 15, "LOAD (L)");
        StdDraw.text(20, 13, "QUIT (Q)");
        StdDraw.show();
    }

    public void startGame() {
        while (!StdDraw.hasNextKeyTyped()) {
        }

        Character firstInput = StdDraw.nextKeyTyped();

        if (firstInput.equals('l') || firstInput.equals('L')) {
            Player p = SerialDemo.loadWorld();
            TERenderer ter = new TERenderer();
            Game game = new Game();
            ter.initialize(game.WIDTH, game.HEIGHT);
            TETile[][] world = p.world;
            ter.renderFrame(world);
            p.move();
            //game.moveAndCursor(p);
            p.quit();
            //game.playWithKeyboard();

        }

        if (firstInput.equals('n') || firstInput.equals('N')) {
            StdDraw.text(20, 11, "ENTER SEED, THEN PRESS S.");
            StdDraw.show();
            String seed = "";
            startHelper(seed, 20);
        }

        if (firstInput.equals('q') || firstInput.equals('Q')) {
            System.exit(0);
        }

    }



    public void startHelper(String seed, double index) {

        while (!StdDraw.hasNextKeyTyped()) {
        }

        Character seedElt = StdDraw.nextKeyTyped();

        if (seedElt.equals('s') || seedElt.equals('S')) {
            TERenderer ter = new TERenderer();
            Game game = new Game();
            ter.initialize(game.WIDTH, game.HEIGHT);
            TETile[][] world = game.playWithInputString("N" + seed + "S");
            ter.renderFrame(world);
            game.playWithKeyboard();
        }

        if (Character.isDigit(seedElt)) {
            StdDraw.text(index, 10, Character.toString(seedElt));
            StdDraw.show();
            startHelper(seed + seedElt, index + .5);
        }

        else {
            startHelper(seed, index);
        }
    }

    public static void main (String[]args){

        Play play = new Play(40, 40);

        play.drawFirstFrame();
        play.startGame();


    }
}