package byog.Core;
import byog.TileEngine.TETile;
import byog.TileEngine.Tileset;
import byog.TileEngine.TERenderer;
import edu.princeton.cs.introcs.StdDraw;
import java.io.Serializable;

public class Player implements Serializable {
    //static int[] playerXY = new int[2];
    int x;
    int y;
    TETile[][] world;
    TERenderer TER = new TERenderer();

    Player(int[] playerXY, TETile[][] world) {
        this.x = playerXY[0];
        this.y = playerXY[1];
        this.world = world;
        world[this.x][this.y] = Tileset.PLAYER;
    }

    public void quit() {

        while (!StdDraw.hasNextKeyTyped()){
        }

        Character quitOption = StdDraw.nextKeyTyped();
        quitHelper(quitOption);
        System.exit(0);
    }

    public void quitHelper(Character quitOption) {

        if (quitOption.equals('q')) {
            SerialDemo.saveWorld(this);
        }
    }


    public void move() {
        while (!StdDraw.hasNextKeyTyped()) {
        }
        Character keyTyped = StdDraw.nextKeyTyped();
        moveHelper(keyTyped);
        TER.renderFrame(world);
        move();

        // MIGHT HAVE TO ALTER THIS SO THAT WE DON'T INFINITELY RECURSE, BECAUSE WE CANNOT ESCAPE THE FUNCTION ONCE ONCE WE CALL MOVE().
    }


    public void moveHelper(Character keyTyped) {
        //String direction = direction(dir);
        if (keyTyped.equals('w')) {
            if (world[x][y + 1].equals(Tileset.FLOOR)) {
                world[x][y] = Tileset.FLOOR;
                world[x][y + 1] = Tileset.PLAYER;
                y++;
            }

        }

        if (keyTyped.equals('s')) {
            if (world[x][y - 1].equals(Tileset.FLOOR)) {
                world[x][y] = Tileset.FLOOR;
                world[x][y - 1] = Tileset.PLAYER;
                y--;
            }

        }

        if (keyTyped.equals('d')) {
            if (world[x + 1][y].equals(Tileset.FLOOR)) {
                world[x][y] = Tileset.FLOOR;
                world[x + 1][y] = Tileset.PLAYER;
                x++;
            }
        }

        if (keyTyped.equals('a')) {
            if (world[x - 1][y].equals(Tileset.FLOOR)) {
                world[x][y] = Tileset.FLOOR;
                world[x - 1][y] = Tileset.PLAYER;
                x--;
            }
        }

        if (keyTyped.equals(':')) {
            quit();
        }
    }
}