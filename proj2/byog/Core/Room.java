package byog.Core;


public class Room {

    private int xStart;
    private int yStart;

    Room(int myXStart, int myYStart) {

        xStart = myXStart;
        yStart = myYStart;
    }

    public int getXStart() {
        return xStart;
    }

    public int getYStart() {
        return yStart;
    }
}

