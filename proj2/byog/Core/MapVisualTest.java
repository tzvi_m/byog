package byog.Core;


import byog.TileEngine.TETile;


public class MapVisualTest {


    public static void main(String[] args) {
//        //initialize the tile rendering engine
//          with a window of size WIDTH x HEIGHT
//        TERenderer ter = new TERenderer();
//
//        ter.initialize(MapGeneratorNew.WIDTH, MapGeneratorNew.HEIGHT);
//
//        long seed = 1234348405;
//        TETile[][] world = MapGeneratorNew.makeWorld(seed);
//
//        ter.renderFrame(world);
        Game game = new Game();
        String seed = "n495234783s";
        TETile[][] world1 = game.playWithInputString(seed);
        TETile[][] world2 = game.playWithInputString(seed);
        System.out.println(worldsAreEqual(world1, world2));
    }

    public static boolean worldsAreEqual(TETile[][] world1, TETile[][] world2) {
        boolean areEqual = true;
        for (int x = 0; x < Game.WIDTH; x += 1) {
            for (int y = 0; y < Game.HEIGHT; y += 1) {
                if (world1[x][y] != world2[x][y]) {
                    areEqual = false;
                }

            }

        }
        return areEqual;
    }
}


