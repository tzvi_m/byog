package byog.Core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import byog.TileEngine.TERenderer;
import byog.TileEngine.TETile;
import byog.TileEngine.TETile;
import edu.princeton.cs.introcs.StdDraw;

import java.util.Random;

public class SerializationTester {  // 1st param - object to serialize, 2nd param - transforming the object into
                                    // sequence of bytes, needs a file -- can save to file or store it in a buffer in memory

    public TETile[][] world;
    public Random random;
//    public long num = Math.round(Math.random()*5000);
//    public String seed = Long.toString(num);

    public void serialize (Play world, File filename) { // breaking up a 2-D array

        // CALL THIS FUNCTION WHEN WE SAVE

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))){
            oos.writeObject(world);
        }
        catch (IOException ex) {
            System.out.println("Error during serialization");
        }

    }




    // OIS knows how to take a sequence of bytes and recreate an object based on this series of bytes.
    // however, it needs a second object that knows how to come up with this series of bytes in the first place --
    // in our case, a file (fileinputstream) --> must tell it where the file is

    public Play deserialize (File filename) {

        // CALL THIS FUNCTION WHEN WE LOAD

        if (filename.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
                Play x = (Play) ois.readObject();
                return x;
            } catch (IOException | ClassNotFoundException ex) {
                System.out.println("Error during deserialization");
            }
        }
        return null;
        //return serialStarter(seed); // make random world if there isn't one to load

    }



    public void serialStarter(Play seed) {



        while (!StdDraw.hasNextKeyTyped()) {}

        Character seedElt = StdDraw.nextKeyTyped();

        if (seedElt.equals('l') || seedElt.equals('L')) {

            TERenderer ter = new TERenderer();

            Game game = new Game();
            ter.initialize(game.WIDTH, game.HEIGHT);


            TETile[][] world = game.playWithInputString("N" + seed + "S");
            //^^this line is why it can be space and not s.


            //calling these in the start function in order to use the same world.
            ter.renderFrame(world);

            game.playWithKeyboard();

        }

    }

}
