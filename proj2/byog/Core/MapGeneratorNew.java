package byog.Core;

import edu.princeton.cs.introcs.StdDraw;
import byog.TileEngine.TETile;
import byog.TileEngine.Tileset;
import java.awt.Font;
import java.io.Serializable;


import java.util.Random;


public class MapGeneratorNew {

    // public static final long SEED = 2873125;
    // public static final Random RANDOM = new Random(SEED);

    private static int roomCap = 40;
    private static Room[] ROOMSARRAY = new Room[roomCap];
    private static int nextRoom = 0;
    private static int amountOfRooms = 0;
    private static TETile[][] emptyWorld(int width, int height) {

        // initialize tiles
        TETile[][] world = new TETile[width][height];
        for (int x = 0; x < width; x += 1) {
            for (int y = 0; y < height; y += 1) {
                world[x][y] = Tileset.NOTHING;
            }
        }
        return world;
    }


    public static void makeRandomNumberOfRooms(TETile[][] world, Random random) {
        int amount = roomCap / 2 + random.nextInt(roomCap / 2);
        for (int i = 0; i < amount; i++) {
            int[] parameterArray = makeRandomRoomParameters(random, Game.WIDTH, Game.HEIGHT);
            if (!overlaps(ROOMSARRAY, parameterArray, world)) {

                int xStart = parameterArray[0];
                int width = parameterArray[1];
                int yStart = parameterArray[2];
                int height = parameterArray[3];

                makeRoom(xStart, width, yStart, height, world);
            }
        }
    }


    public static void connectRooms(TETile[][] world) {
        for (int i = 0; i < amountOfRooms - 1; i++) {
            //iterates to second to last room and connects it to last room, if both are islands.
            connect(ROOMSARRAY[i], ROOMSARRAY[i+1], world);
        }
    }

    private static void connect(Room r1, Room r2, TETile[][] world) {

        /**creates starting and ending points for the hall.
         in this preliminary version, halls only go from the bottom
         left corner of rooms to the bottom left corner of other rooms. */

        int xBegin = Math.min(r1.getXStart(), r2.getXStart());
        int xEnd = Math.max(r1.getXStart(), r2.getXStart());
        int yBegin = Math.min(r1.getYStart(), r2.getYStart());
        int yEnd = Math.max(r1.getYStart(), r2.getYStart());
        for (int x = xBegin; x <= xEnd; x++) {

            world[x][yBegin] = Tileset.FLOOR;
        }

        for (int y = yBegin; y <= yEnd; y++) {

            world[xEnd][y] = Tileset.FLOOR;
        }
    }

    public static void makeWalls(TETile[][] world, int width, int height){
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (world[i][j].equals(Tileset.FLOOR)) {
                    if (world[i+1][j].equals(Tileset.NOTHING)){
                        world[i+1][j] = Tileset.WALL;
                    }
                    if (world[i-1][j].equals(Tileset.NOTHING)){
                        world[i-1][j] = Tileset.WALL;
                    }
                    if (world[i][j+1].equals(Tileset.NOTHING)){
                        world[i][j+1] = Tileset.WALL;
                    }
                    if (world[i][j-1].equals(Tileset.NOTHING)){
                        world[i][j-1] = Tileset.WALL;
                    }
                    if (world[i+1][j+1].equals(Tileset.NOTHING)){
                        world[i+1][j+1] = Tileset.WALL;
                    }
                    if (world[i+1][j-1].equals(Tileset.NOTHING)){
                        world[i+1][j-1] = Tileset.WALL;
                    }
                    if (world[i-1][j+1].equals(Tileset.NOTHING)){
                        world[i-1][j+1] = Tileset.WALL;
                    }
                    if (world[i-1][j-1].equals(Tileset.NOTHING)){
                        world[i-1][j-1] = Tileset.WALL;
                    }
                }
            }
        }
    }


    public static boolean overlaps(Room[] roomsArray, int[] parameterArray, TETile[][] world) {

        //new array that contains (in order) :
        // x lower bound, x upper bound, y lower bound, y upper bound.
        int[] boundsArray = {parameterArray[0], parameterArray[0] + parameterArray[1],
                parameterArray[2], parameterArray[2] + parameterArray[3]};

        boolean doesOverlap = false;

        for (int l = boundsArray[0] - 1; l <= boundsArray[1]; l++) {
            for (int h = boundsArray[2] - 1; h <= boundsArray[3]; h++) {

                //if (l == j && h == k) {
                if (world[l][h] == Tileset.FLOOR) {
                    doesOverlap = true;
                }

            }

        }
        return doesOverlap;
    }

    public static int[] makeRandomRoomParameters(Random random, int width, int height) {

        // returns an array of the randomized parameters for a single random room,
        // which will be checked for overlap in the overlaps function
        // before being made into a random room.
        // this array consists of: 0: xStart, 1: width, 2: yStart, 3; height.

        int myHeight = 4 + random.nextInt(8);
        int myWidth = 4 + random.nextInt(8);

        //xMax is the farthest right we can start,
        // without building part of the room outside our window.
        int xMax = width - myWidth - 2;
        int xStart = 2 + random.nextInt(xMax);


        // yMax is the farthest up we can start,
        // without building part of the room outside our window.
        int yMax = height - myHeight - 2;
        int yStart = 2 + random.nextInt(yMax);

        int[] parameterArray = {xStart, myWidth, yStart, myHeight};

        return parameterArray;


        //makeRoom(myWidth, myHeight, xStart, yStart, world);
    }


    public static void makeRoom(int xStart, int width, int yStart, int height, TETile[][] world) {
        for (int y = yStart; y < yStart + height - 1; y++) {
            for (int x = xStart; x < xStart + width - 1; x++) {
                world[x][y] = Tileset.FLOOR;
            }
        }
        ROOMSARRAY[nextRoom] = new Room(xStart, yStart);
        nextRoom++;
        amountOfRooms++;
    }

    public static TETile[][] makeWorld(Random random) {

        TETile[][] world = emptyWorld(Game.WIDTH, Game.HEIGHT);

        //make rooms, halls, and walls!
        makeRandomNumberOfRooms(world, random);
        connectRooms(world);
        makeWalls(world, Game.WIDTH, Game.HEIGHT);


        //reset static variables.
        roomCap = 40;
        ROOMSARRAY = new Room[roomCap];
        nextRoom = 0;
        amountOfRooms = 0;
        return world;

    }

    public static int[] makeRandomPlayerPosition(Random random, TETile[][] world) {
        int randomX = RandomUtils.uniform(random, Game.WIDTH);
        int randomY = RandomUtils.uniform(random, Game.HEIGHT);

        if (world[randomX][randomY].equals(Tileset.FLOOR)) {
            int[] randomXY = {randomX , randomY};
            return randomXY;
        }

        return makeRandomPlayerPosition(random, world);
    }


    public static int[] cursorLocation() {
        int x = (int) Math.round(StdDraw.mouseX());
        int y = (int) Math.round(StdDraw.mouseY());
        int[] cursorXY = {x , y};
        return cursorXY;
    }


    public static String tileType(int x, int y, TETile[][] world) {
        String type = new String();

        if (world[x][y].equals(Tileset.FLOOR)) {
            type = "Floor";

        } else if (world[x][y].equals(Tileset.NOTHING)) {
            type = "Nothing";


        } else if (world[x][y].equals(Tileset.WALL)) {
            type = "Wall";

        } else if (world[x][y].equals(Tileset.PLAYER)) {
            type = "Player";
        }

        return type;
    }

    public static void displayCursorLocation(TETile[][] world) {
        Font font = new Font("Monaco", Font.BOLD, 12);
        StdDraw.setFont(font);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.filledRectangle(0, Game.HEIGHT - 1, 11, 1);
        displayCursorLocationHelper(world);

    }

    public static void displayCursorLocationHelper(TETile[][] world) {
        int[] cursorXY = cursorLocation();
        String tileType = tileType(cursorXY[0], cursorXY[1], world);
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.text(4, Game.HEIGHT - 1, tileType);
        StdDraw.show();

    }
}
