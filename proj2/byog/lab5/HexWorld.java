package byog.lab5;
import org.junit.Test;
import static org.junit.Assert.*;

import byog.TileEngine.TERenderer;
import byog.TileEngine.TETile;
import byog.TileEngine.Tileset;

import java.util.Random;

/**
 * Draws a world consisting of hexagonal regions.
 */
public class HexWorld {


    // fills in a block 15 tiles wide by 5 tiles tall
    //        for (int x = 2; x < 17; x += 1) {
    //            for (int y = 2; y < 7; y += 1) {
    //                world[x][y] = Tileset.TREE;
    //            }
    //        }


    private static final int WIDTH = 60;
    private static final int HEIGHT = 30;



//    public static void Hex(int size, int xp, int yp) {
//        TETile[][] world = new TETile[WIDTH][HEIGHT];
//        for (int x = s-1; x <= s; x++ ) {
//            for (int y = 0; y <= s*2; y++) {
//                world[x][y] = Tileset.TREE;
//            }
//        }
//
//    }

    public static void main(String[] args) {
        // initialize the tile rendering engine with a window of size WIDTH x HEIGHT
        TERenderer ter = new TERenderer();
        ter.initialize(WIDTH, HEIGHT);

        // initialize tiles
        TETile[][] world = new TETile[WIDTH][HEIGHT];
        for (int x = 0; x < WIDTH; x += 1) {
            for (int y = 0; y < HEIGHT; y += 1) {
                world[x][y] = Tileset.NOTHING;
            }
        }

        // draws the world to the screen
        ter.renderFrame(world);

    }

}
